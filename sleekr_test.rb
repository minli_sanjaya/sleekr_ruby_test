# define question words, grouped by level
@level1 = ['buku','soda','jari','bayi']
@level2 = ['minum','pergi','ambil','ketuk']
@level3 = ['pulang','sampai','hutang','bicara']

# declare initial variable
@answer
@poin = 0

# function to pick random answer based on level
def pickAnswerByLevel(level)
	if level == 1
		puts 'Anda memilih level 1'
		return @level1.sample()
	elsif level == 2
		puts 'Anda memilih level 2'
		return @level2.sample()
	elsif level == 3
		puts 'Anda memilih level 3'
		return @level3.sample()
	else
		puts 'Input level game hanya 1 - 3'
	end
end

# function to validate user answer input
def validateAnswer(input, answer)
	if input == answer
		@poin += 1
		puts 'BENAR point anda: ' + @poin.to_s
		return true
	else
		puts 'SALAH! Silakan coba lagi'
		return false
	end
end

# start game codes
puts 'Sleekr Ruby Test Game'
puts '====================='
loop do
	while @answer.nil?
		print 'Pilih level game [1..3]: '

		inputLevel = gets.chomp

		@answer = pickAnswerByLevel(inputLevel.to_i)	
	end
	
	# shuffle answer characters to make question
	question = @answer.chars.shuffle.join

	loop do
		puts 'Tebak kata: ' + question
		
		print 'Jawab: '

		inputAnswer = gets.chomp
	
		break if validateAnswer(inputAnswer, @answer)
	end
	
	puts '====================='
	puts '====================='
	
	@answer = nil
	
	break if @poin > 10
end

puts 'Thanks for playing...'